#!/bin/bash
#code couleur
rouge='\e[1;31m'
jaune='\e[1;33m'
bleu='\e[1;34m'
green='\e[4;32m' #souligne en vert
neutre='\e[0;m'
# Vérification que le script est lancée sans sudo
if [ "$UID" -eq "0" ]
then
	echo -e "${rouge}lance le sans sudo le mots de passe sera demandé dans le terminal lors de la 1ère action néccessitant le droit administrateur.${green}"
	exit
fi
if [ ! -f "/usr/bin/wget" ]
then
	echo -e "${green}installation de wget pour l'installation des paquets deb${neutre}"
	sudo apt install gdebi-core wget
else
	echo "wget est déja installer"
fi
if [ ! -f "/usr/bin/qcma" ]
then
	echo "installation de qcma"
	sudo sh -c "echo 'deb https://download.opensuse.org/repositories/home:/codestation/xUbuntu_18.04/ /'> /etc/apt/sources.list.d/home:codestation.list"
	wget -nv https://download.opensuse.org/repositories/home:/codestation/xUbuntu_18.04/Release.key -O Release.key
	sudo apt-key - < Release.key
	sudo apt update
	sudo apt-get install qcma
else
	echo -e "${rouge}qcma est déja installer${neutre}"
	echo "le script continue la vérification et l'installation des programmes"
fi
#installation de vscodium (forklibre de visualcode)
if [ ! -f "/usr/bin/codium" ]
then
echo "${bleu}installation de vscodium"
wget -qO - https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg | sudo apt-key add - 
echo 'deb https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/repos/debs/ vscodium main' | sudo tee --append /etc/apt/sources.list.d/vscodium.list 
sudo apt update && sudo apt install codium 
else 
	echo -e "${rouge}codium est déja installer${neutre}"
	echo "le script continue la vérification et l'installation des programmes"
fi
if [ ! -f "/usr/bin/mumble" ]
then
echo "${bleu}installation de mumble${neutre}"
sudo apt install mumble
else
	echo -e "${rouge}mumble est déjà installer${neutre}"
	echo "le script continue la vérification et l'installation des programmes"
fi
if [ ! -f "/usr/bin/vlc" ]
then
echo "${jaune}installation de vlc${neutre}"
sudo apt install vlc
else
	echo -e "${rouge}vlc est déja installer${neutre}"
	echo "le script continue la vérification et l'installation des programmes"
fi
if [ ! -f "/usr/bin/discord" ]
then
	echo -e "${bleu}installation de discord${neutre}"
$ wget -O ~/discord.deb "https://discordapp.com/api/download?platform=linux&format=deb"
sudo gdebi ~/discord.deb
else
	echo -e "${rouge}Discord est déja installer${neutre}"
fi
